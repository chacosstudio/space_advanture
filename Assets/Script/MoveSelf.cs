﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MoveSelf : MonoBehaviour {

    public float smoothing;
    public float zMoveMax;
    public float zMoveMin;

    private float moveSpeed;
    public bool up;

    private Rigidbody rb;
    private GameController gameController;

    
    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent <GameController>();
        }
        if (gameController == null)
        {
            Debug.Log ("Cannot find 'GameController' script");
        }
		rb=GetComponent<Rigidbody>();
        up=true;
        moveSpeed=0.5f;
    }

	void FixedUpdate ()
    {   
        int timePast=(int)Math.Floor(Time.timeSinceLevelLoad);
        float backPoint=0.8f;
        if(timePast%20==0)
        {    
            moveSpeed=1.5f;
        }
        if(timePast>=20)
        {
            backPoint=1.2f;
        }
        
        if(up){
            // float newPosition = Mathf.MoveTowards (rb.position.z, zMoveMax, Time.deltaTime * Random.Range (0.1f, moveSpeed)); 
            // rb.position=new Vector3(rb.position.x,rb.position.y,newPosition);
            var locVel = transform.InverseTransformDirection(rb.velocity);
            locVel.y = moveSpeed;
            rb.velocity = transform.TransformDirection(locVel);
            if(Mathf.Abs(transform.position.z-zMoveMax)<=backPoint){
                up=false;
         }
        }
        else{
            // float newPosition = Mathf.MoveTowards (rb.position.z, zMoveMin, Time.deltaTime * Random.Range (0.1f, moveSpeed));
            // rb.position=new Vector3(rb.position.x,rb.position.y,newPosition);
            var locVel = transform.InverseTransformDirection(rb.velocity);
            locVel.y = -moveSpeed;
            rb.velocity = transform.TransformDirection(locVel);
            if(Mathf.Abs(transform.position.z-zMoveMin)<=backPoint) {
                up=true;
            }
        }

    }
}
