﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	[Header("Jump up")]	
	public float power;
	Rigidbody rb;

    public bool JumpKey
    {
        get
        {
            foreach (Touch touch in Input.touches) {
                if (touch.phase == TouchPhase.Began) {
                    return true;
                }
            }
            return Input.GetKey(KeyCode.Space);
        }
    }
	void TryJump()
    {
        if (JumpKey)
        {
            rb.velocity=new Vector3(0,0,power);
        }
    }


	void Start () {
		rb=GetComponent<Rigidbody>();
		Physics.gravity = new Vector3(0, 0, -4.9f);
	}
	
	// Update is called once per frame
	void Update () {
			TryJump();
	}
	
}
