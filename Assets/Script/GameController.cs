﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;


public class GameController : MonoBehaviour {

	public GameObject[] tubeFromBottom;

	public GameObject[] tubeFromTop;
    
    public Transform spawnPositionTop;
    public Transform spawnPositionBottom;

    public float spawnWait;
    public float startWait;
    public Text scoreText;
    public Text restartText;
    public Text gameOverText;

    private bool gameOver;
    private bool restart;

    public float tubePositionChangeNum;
    private float zTubeTopGeneratePosition;
    private float zTubePositionRange;//position "tubePositionChangeNum" per spawnWait

    private bool zTubePositionPlusOrSub;//position +/-0.1f 
    //public float intervalNum;
    private int tubeReferenceTop;
    private int tubeReferenceBottom;
    public int tubeRotation;


    void Start ()
    {
        gameOver = false;
        restart = false;
        restartText.text = "";
        scoreText.text="Score:"+0;
        gameOverText.text = "";
        StartCoroutine (SpawnWaves ());
        zTubeTopGeneratePosition=UnityEngine.Random.Range(2f,8f);
        zTubePositionRange=zTubeTopGeneratePosition;
        zTubePositionPlusOrSub=true;
        tubeReferenceTop=0;
        tubeReferenceBottom=0;



    }

    void Update ()
    {
        if (restart)
        {
            bool screenTouched = false;
            foreach (Touch touch in Input.touches) {
                if (touch.phase == TouchPhase.Began) {
                    screenTouched |= true;
                }
            }
            if (Input.GetKeyDown (KeyCode.R) || screenTouched)
            {
                SceneManager.LoadScene("StartMenu", LoadSceneMode.Single);
                //Application.LoadLevel (Application.loadedLevel);
            }
        }
        UpdateScore();


    }

    IEnumerator SpawnWaves ()
    {
        yield return new WaitForSeconds (0);
        while (true)
        {       
                //zTubeTopGeneratePosition=UnityEngine.Random.Range(1.5f,8.5f);

                System.Random ran = new System.Random();
                int time=(int)Math.Floor(Time.timeSinceLevelLoad);
                spawnWait=UnityEngine.Random.Range(2f,6f);

                Vector3 spawnPositionForTop = new Vector3 (spawnPositionTop.position.x, spawnPositionTop.position.y,zTubePositionRange);
                Vector3 spawnPositionForBottom = new Vector3 (spawnPositionBottom.position.x, spawnPositionBottom.position.y,-(10-zTubePositionRange));

                if(time>=20){
                    Debug.Log("Enter");
                    spawnWait=UnityEngine.Random.Range(3f,5f);
                    tubeReferenceTop=ran.Next(0,2);
                    tubeReferenceBottom=tubeReferenceTop;
                }
                if(time>=60){
                    spawnWait=UnityEngine.Random.Range(1.5f,4f);
                    tubeReferenceTop=2;
                    tubeReferenceBottom=2;
                }
                Quaternion tubeRotationTop = tubeFromTop[tubeReferenceTop].transform.rotation;
                Quaternion tubeRotationButtom = tubeFromBottom[tubeReferenceBottom].transform.rotation;
                Instantiate (tubeFromTop[tubeReferenceTop], spawnPositionForTop, tubeRotationTop);
                Instantiate (tubeFromBottom[tubeReferenceBottom], spawnPositionForBottom, tubeRotationButtom);
 



                yield return new WaitForSeconds (spawnWait);//interval of generating each tube

                zTubePositionPlusOrSub=IsTubeOverBoundary(zTubePositionRange,zTubePositionPlusOrSub);

                if(zTubePositionPlusOrSub)
                {
                    zTubePositionRange+=tubePositionChangeNum;
                }
                else if(!zTubePositionPlusOrSub)
                {
                    zTubePositionRange-=tubePositionChangeNum;
                }

                if (gameOver)
                {
                    restartText.text = "Press 'R' for Restart";
                    restart = true;
                    //break;
                }

        }
    }

    void UpdateScore ()
    {
        //scoreText.text = "Score: " + (DateTime.Now.Second).ToString ();
        if(!gameOver)
        scoreText.text = "Score: " + Math.Floor(Time.timeSinceLevelLoad);


    }

    public void GameOver ()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
        Debug.Log(gameOver);
    }

    bool IsTubeOverBoundary(float currentPosition,bool currentBool){

                 if(Mathf.Abs(currentPosition-8f)<=tubePositionChangeNum)
                {
                    return false;
                }
                else if(Mathf.Abs(currentPosition-2f)<=tubePositionChangeNum){
                    return true;
                }
            return currentBool;
    }
}


