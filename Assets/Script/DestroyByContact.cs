﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {
    private GameController gameController;

    public GameObject playerExplosion;
    //private int gameOverCount=5;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent <GameController>();
        }
        if (gameController == null)
        {
            Debug.Log ("Cannot find 'GameController' script");
        }
    }
    void OnTriggerEnter (Collider other)
    {
        if (other.CompareTag ("Wall")||other.CompareTag("Tube") )
        {
            return;
        }
        if (other.tag == "Player")
        {
            // gameOverCount-=1;
            // if(gameOverCount<=0){
                Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
                gameController.GameOver();
            // }
        
        }
        
        Destroy (other.gameObject);
    }
}
